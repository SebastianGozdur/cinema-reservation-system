package pl.sgozdur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.sgozdur.core.CommandGateway;
import pl.sgozdur.reservation.SeatAvailabilityChecker;
import pl.sgozdur.reservation.commands.CreateReservationCommand;
import pl.sgozdur.reservation.handlers.CreateReservationHandler;

@SpringBootApplication
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

    @Bean
    public SeatAvailabilityChecker createSeatAvailabilityChecker() {
        return new SeatAvailabilityChecker();
    }

    @Bean
    public CreateReservationHandler createHandler() {
        return new CreateReservationHandler();
    }

    @Bean
    public CommandGateway commandGateway(CreateReservationHandler createReservationHandler) {
        CommandGateway commandGateway = new CommandGateway();

        commandGateway.registerHandler(new CreateReservationCommand(), createReservationHandler);

        return commandGateway;
    }
}
