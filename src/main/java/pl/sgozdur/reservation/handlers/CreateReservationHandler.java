package pl.sgozdur.reservation.handlers;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sgozdur.core.Command;
import pl.sgozdur.core.Handler;
import pl.sgozdur.model.Reservation;
import pl.sgozdur.model.Seat;
import pl.sgozdur.reservation.ReservationMapper;
import pl.sgozdur.reservation.ReservationRepository;
import pl.sgozdur.reservation.SeatAvailabilityChecker;
import pl.sgozdur.seat.SeatExtractor;
import pl.sgozdur.seat.SeatRepository;
import pl.sgozdur.reservation.commands.CreateReservationCommand;
import pl.sgozdur.seat.custom_exceptions.SeatOccupiedException;

import java.util.List;

public class CreateReservationHandler implements Handler {

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private SeatRepository seatRepository;
    @Autowired
    private SeatAvailabilityChecker seatAvailabilityChecker;

    @Override
    public void handle(Command command) throws SeatOccupiedException {
        CreateReservationCommand createReservationCommand = (CreateReservationCommand) command;

        ObjectId id = ObjectId.get();
        Reservation reservation = new ReservationMapper().mapCreationCommand(createReservationCommand);
        reservation.setId(id);

        SeatExtractor seatExtractor = new SeatExtractor();
        List<Seat> seats = seatExtractor.connectsSeatsToReservation(reservation, createReservationCommand.getSeats());
        seats = seatExtractor.connectSeatsToMovieSessions(reservation, createReservationCommand.getSeats());

        seatAvailabilityChecker.checkFieldAvailability(reservation.getMovieSessionId(), seats);

        reservationRepository.save(reservation);
        seatRepository.saveAll(seats);

        //send an email
    }
}
