package pl.sgozdur.reservation;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import pl.sgozdur.model.Reservation;

public interface ReservationRepository extends MongoRepository<Reservation, String> {

    Reservation findById(ObjectId id);
}
