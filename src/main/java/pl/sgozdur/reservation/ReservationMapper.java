package pl.sgozdur.reservation;

import pl.sgozdur.model.Reservation;
import pl.sgozdur.reservation.commands.CreateReservationCommand;

public class ReservationMapper {

    public Reservation mapCreationCommand(CreateReservationCommand createReservationCommand) {
        Reservation reservation = new Reservation();

        reservation.setCinemaName(createReservationCommand.getCinemaName());
        reservation.setNumberOfSeats(createReservationCommand.getNumberOfSeats());
        reservation.setStartDate(createReservationCommand.getStartDate());
        reservation.setMovieSessionId(createReservationCommand.getMovieSession());

        return reservation;
    }
}
