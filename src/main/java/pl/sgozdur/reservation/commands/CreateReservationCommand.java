package pl.sgozdur.reservation.commands;

import org.bson.types.ObjectId;
import pl.sgozdur.core.Command;
import pl.sgozdur.model.Seat;

import java.time.LocalDateTime;
import java.util.List;

public class CreateReservationCommand implements Command {

    public ObjectId movieSession;
    public String cinemaName;
    public LocalDateTime startDate;
    public int numberOfSeats;
    public List<Seat> seats;

    public ObjectId getMovieSession() {
        return movieSession;
    }

    public void setMovieSession(ObjectId movieSession) {
        this.movieSession = movieSession;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
