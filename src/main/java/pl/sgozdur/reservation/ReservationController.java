package pl.sgozdur.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.sgozdur.Utils.Messages;
import pl.sgozdur.core.CommandGateway;
import pl.sgozdur.reservation.commands.CreateReservationCommand;

@RestController
public class ReservationController {

    @Autowired
    CommandGateway commandGateway;

    @PostMapping("/reservation/make")
    public String create(@RequestBody CreateReservationCommand createReservationCommand) {
        String message;

        try {
            commandGateway.execute(createReservationCommand);
            message = Messages.RESERVATION_CREATED;
        } catch(Exception exception) {
            message = exception.getMessage();
        }

        return message;
    }
}
