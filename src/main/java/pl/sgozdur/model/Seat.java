package pl.sgozdur.model;

import org.bson.types.ObjectId;

public class Seat {

    ObjectId reservationId;
    ObjectId movieSessionId;
    private int seatNumber;
    private int row;

    public Seat() {}

    public Seat(int row, int seatNumber) {
        this.row = row;
        this.seatNumber = seatNumber;
    }

    public ObjectId getReservationId() {
        return reservationId;
    }

    public void setReservationId(ObjectId reservationId) {
        this.reservationId = reservationId;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public ObjectId getMovieSessionId() {
        return movieSessionId;
    }

    public void setMovieSessionId(ObjectId movieSessionId) {
        this.movieSessionId = movieSessionId;
    }

    @Override
    public boolean equals(Object object) {
        Seat seat = (Seat) object;

        if(this.getRow() == seat.getRow() && this.getSeatNumber() == seat.getSeatNumber()) {
            return true;
        }

        return false;
    }
}
