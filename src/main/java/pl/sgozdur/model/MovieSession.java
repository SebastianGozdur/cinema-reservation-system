package pl.sgozdur.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.util.List;

public class MovieSession {

    @Id
    private ObjectId id;

    private String movieName;
    private LocalDate sessionStart;
    private int sessionLength;

    public MovieSession() {}

    public MovieSession(String movieName, LocalDate sessionStart, int sessionLength) {
        this.movieName = movieName;
        this.sessionStart = sessionStart;
        this.sessionLength = sessionLength;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public LocalDate getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(LocalDate sessionStart) {
        this.sessionStart = sessionStart;
    }

    public int getSessionLength() {
        return sessionLength;
    }

    public void setSessionLength(int sessionLength) {
        this.sessionLength = sessionLength;
    }
}
