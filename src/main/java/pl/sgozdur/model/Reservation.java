package pl.sgozdur.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.List;

public class Reservation {

    @Id
    private ObjectId id;

    private String cinemaName;
    private ObjectId movieSessionId;
    private int cost;
    private LocalDateTime startDate;
    private int numberOfSeats;
    private List<Seat> seats;

    public Reservation() {}

    public Reservation(ObjectId id, String cinemaName, ObjectId movieSessionId, int cost, LocalDateTime startDate, int numberOfSeats, List<Seat> seats) {
        this.id = id;
        this.cinemaName = cinemaName;
        this.movieSessionId = movieSessionId;
        this.cost = cost;
        this.startDate = startDate;
        this.numberOfSeats = numberOfSeats;
        this.seats = seats;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public ObjectId getMovieSessionId() {
        return movieSessionId;
    }

    public void setMovieSessionId(ObjectId movieSessionId) {
        this.movieSessionId = movieSessionId;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
