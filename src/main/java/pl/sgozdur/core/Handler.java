package pl.sgozdur.core;

import pl.sgozdur.seat.custom_exceptions.SeatOccupiedException;

public interface Handler {

    void handle(Command command) throws SeatOccupiedException;
}
