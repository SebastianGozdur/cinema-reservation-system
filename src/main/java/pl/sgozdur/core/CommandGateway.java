package pl.sgozdur.core;

import pl.sgozdur.seat.custom_exceptions.SeatOccupiedException;

import java.util.HashMap;
import java.util.Map;

public class CommandGateway {

    private Map<String , Handler> availableHandlers = new HashMap<String, Handler>();

    public void registerHandler(Command command, Handler handler) {
        this.availableHandlers.put(command.getClass().getTypeName(), handler);
    }

    public void execute(Command command) throws SeatOccupiedException {
        Handler handler = availableHandlers.get(command.getClass().getTypeName());
        handler.handle(command);
    }

    public boolean isHandlerAvailable(Command command) {
        System.out.println(this.availableHandlers.keySet());
        if (this.availableHandlers.containsKey(command.getClass().getTypeName())) {
            return true;
        }

        return false;
    }
}
