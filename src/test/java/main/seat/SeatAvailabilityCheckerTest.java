package main.seat;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import pl.sgozdur.model.MovieSession;
import pl.sgozdur.model.Seat;
import pl.sgozdur.reservation.SeatAvailabilityChecker;
import pl.sgozdur.seat.SeatRepository;
import pl.sgozdur.seat.custom_exceptions.SeatOccupiedException;

import java.util.ArrayList;
import java.util.Arrays;

public class SeatAvailabilityCheckerTest {

    @Mock
    private SeatAvailabilityChecker seatAvailabilityChecker;

    @Mock
    private SeatRepository seatRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testScenarioWhereSeatIsOccupied() throws SeatOccupiedException {
        //given
        MovieSession movieSession = new MovieSession();
        Seat existingSeat = new Seat(10, 32);
        existingSeat.setMovieSessionId(movieSession.getId());

        Mockito.when(seatRepository.findByMovieSessionId(movieSession.getId())).thenReturn(new ArrayList<Seat>(Arrays.asList(existingSeat)));

        Seat seatToAdd = new Seat(10,32);

        seatAvailabilityChecker.checkFieldAvailability(movieSession.getId(), new ArrayList<>(Arrays.asList(seatToAdd)));

        //when //then
        Assertions.assertThrows(SeatOccupiedException.class, () -> {
            seatAvailabilityChecker.checkFieldAvailability(movieSession.getId(), new ArrayList<>(Arrays.asList(seatToAdd)));
        });
    }
}
