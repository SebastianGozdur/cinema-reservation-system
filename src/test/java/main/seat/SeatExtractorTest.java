package main.seat;

import org.junit.Assert;
import org.junit.Test;
import pl.sgozdur.model.Reservation;
import pl.sgozdur.model.Seat;
import pl.sgozdur.seat.SeatExtractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SeatExtractorTest {

    private SeatExtractor seatExtractor = new SeatExtractor();

    @Test
    public void testReservationAndSeatsConnecting() {
        //given
        Seat seat = new Seat(10, 32);
        Reservation reservation = new Reservation();

        //when
        List<Seat> result = seatExtractor.connectsSeatsToReservation(reservation, new ArrayList<Seat>(Arrays.asList(seat)));

        //then
        Assert.assertEquals(reservation.getId(), result.get(0).getReservationId());
    }

    @Test
    public void testMovieSessionAndSeatsConnecting() {
        //given
        Seat seat = new Seat(10, 32);
        Reservation reservation = new Reservation();

        //when
        List<Seat> result = seatExtractor.connectsSeatsToReservation(reservation, new ArrayList<Seat>(Arrays.asList(seat)));

        //then
        Assert.assertEquals(reservation.getMovieSessionId(), result.get(0).getMovieSessionId());
    }
}
