package main.reservation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.sgozdur.model.MovieSession;
import pl.sgozdur.model.Reservation;
import pl.sgozdur.model.Seat;
import pl.sgozdur.moviesession.MovieSessionRepository;
import pl.sgozdur.reservation.ReservationMapper;
import pl.sgozdur.reservation.commands.CreateReservationCommand;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReservationMapperTest {

    private ReservationMapper reservationMapper = new ReservationMapper();

    @Mock
    MovieSessionRepository movieSessionRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReservationMapping() {
        //given
        CreateReservationCommand createReservationCommand = new CreateReservationCommand();
        MovieSession movieSession = new MovieSession("Avengers: Endgame", null, 120);
        Mockito.when(movieSessionRepository.findByMovieName("Avengers: Endgame")).thenReturn(new ArrayList<MovieSession>(Arrays.asList(movieSession)));
        List<MovieSession> movieSessions = movieSessionRepository.findByMovieName("Avengers: Endgame");

        Seat seat = new Seat(10, 32);
        List<Seat> seats = new ArrayList<Seat>(Arrays.asList(seat));

        LocalDateTime localDateTime = LocalDateTime.of(1, 12, 15, 10, 45);;
        createReservationCommand.movieSession = movieSessions.get(0).getId();
        createReservationCommand.cinemaName = "test";
        createReservationCommand.startDate = localDateTime;
        createReservationCommand.seats = seats;

        //when
        Reservation result = reservationMapper.mapCreationCommand(createReservationCommand);

        //then
        Assert.assertEquals(movieSessions.get(0).getId(), result.getId());
        Assert.assertEquals("test", result.getCinemaName());
        Assert.assertEquals(localDateTime, result.getStartDate());
    }
}
