package main.moviesession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.sgozdur.model.MovieSession;
import pl.sgozdur.moviesession.MovieSessionController;
import pl.sgozdur.moviesession.MovieSessionRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MovieSessionTest {

    @Mock
    MovieSessionController movieSessionController;

    @Mock
    MovieSessionRepository movieSessionRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testMovieSessionsFinder() {
        //given
        MovieSession movieSession = new MovieSession();
        Mockito.when(movieSessionRepository.findAll()).thenReturn(new ArrayList<MovieSession>(Arrays.asList(movieSession)));
        Mockito.when(movieSessionController.getAll()).thenReturn(new ArrayList<MovieSession>(Arrays.asList(movieSession)));

        //when
        List<MovieSession> result = movieSessionController.getAll();

        //then
        Assert.assertNotEquals(0, result.size());
        Assert.assertEquals(1, result.size());
    }
}
