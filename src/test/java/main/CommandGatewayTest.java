package main;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.junit.jupiter.api.Assertions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sgozdur.core.CommandGateway;
import pl.sgozdur.model.MovieSession;
import pl.sgozdur.model.Seat;
import pl.sgozdur.moviesession.MovieSessionRepository;
import pl.sgozdur.reservation.ReservationController;
import pl.sgozdur.reservation.commands.CreateReservationCommand;
import pl.sgozdur.reservation.handlers.CreateReservationHandler;
import pl.sgozdur.seat.custom_exceptions.SeatOccupiedException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandGatewayTest {

    private CommandGateway commandGateway = new CommandGateway();

    @Autowired
    private CreateReservationHandler createReservationHandler;

    @Mock
    MovieSessionRepository movieSessionRepository;

    @Autowired
    ReservationController reservationController;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandlerRegistration() {
        //given
        CreateReservationCommand command = new CreateReservationCommand();
        CreateReservationHandler handler = new CreateReservationHandler();

        commandGateway.registerHandler(command, handler);

        //when
        boolean result = commandGateway.isHandlerAvailable(command);

        //then
        Assert.assertEquals(true, result);
    }

    @Test
    public void testSuccessfulReservationCreation() {
        //given
        MovieSession movieSession = new MovieSession("Avengers: Endgame", null, 120);
        Mockito.when(movieSessionRepository.findByMovieName("Avengers: Endgame")).thenReturn(new ArrayList<MovieSession>(Arrays.asList(movieSession)));

        CreateReservationCommand createReservationCommand = new CreateReservationCommand();
        CreateReservationHandler createReservationHandler = new CreateReservationHandler();
        List<MovieSession> movieSessions = movieSessionRepository.findByMovieName("Avengers: Endgame");
        Seat seat = new Seat(10, 32);
        List<Seat> seats = new ArrayList<Seat>();
        seats.add(seat);

        commandGateway.registerHandler(createReservationCommand, createReservationHandler);


        createReservationCommand.movieSession = movieSessions.get(0).getId();
        createReservationCommand.cinemaName = "test";
        createReservationCommand.startDate = LocalDateTime.of(1, 12, 15, 10, 45);
        createReservationCommand.seats = seats;
    }
}
